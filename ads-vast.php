<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>VAST-integrated Brightcove Player</title>

    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css">
    <link href="../player-plugins/videojs-contrib-ads/videojs.ads.css" rel="stylesheet" type="text/css">
    <link href="../player-plugins/videojs-vast/videojs.vast.css" rel="stylesheet" type="text/css">

    <script src="js/vendor/modernizr.js"></script>
    <link rel="shortcut icon" href="favicon.ico">
  </head>

  <body>

<?php include_once("google-tag-manager.php.inc"); ?>

<?php include_once("menu.php.inc"); ?>

    <div class="row">
      <div class="large-12 columns">
        <h1>Ad-integrated Brightcove Player with a FOSS VAST plugin</h1>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
      	<div class="panel">
	        <h3>Amazing to see you here!</h3>
	        <p>
            Brightcove Player with VAST ads integrated.
            We use <a href="https://github.com/theonion/videojs-vast-plugin">videojs-vast-plugin</a> here.
          </p>


            <div class="outer">

<video id="example_video_1"
  data-video-id="4333796665001"
  data-account="4143377145001"
  data-player="36d87657-6087-469d-bc72-8deb7145facd"
  data-embed="default"
  class="video-js" controls></video>
<script src="//players.brightcove.net/4143377145001/36d87657-6087-469d-bc72-8deb7145facd_default/index.min.js"></script>


              <p><div class="fb-like" data-send="false" data-width="450" data-show-faces="true" data-colorscheme="dark" data-font="trebuchet ms"></div></p>

            </div>

          <p>Feel free to explore the source code of this page.</p>
        </div>

        <div class="panel">
          <h3>Learn more</h3>
          <form action="http://support.brightcove.com/en/video-cloud/search/"  accept-charset="UTF-8" method="post">
            <div class="row">
              <div class="large-12 columns">
                <div class="row collapse">
                  <div class="large-10 columns">
                    <input type="text" placeholder="Have a question about Brightcove?" name="search_block_form">
                  </div>
                  <div class="large-2 columns">
                    <button type="submit" class="button prefix">Search</button>
                    <input type="hidden" name="form_build_id" id="form-1b2d71d83ffa155abd063cae196a7104" value="form-1b2d71d83ffa155abd063cae196a7104"  />
                    <input type="hidden" name="form_id" id="edit-search-block-form" value="search_block_form"  />
                  </div>
                </div>
              </div>
            </div>
          </form>

	        <div class="row">
	        	<div class="large-3 medium-3 columns">
    	    		<p><a href="https://gitlab.com/bc/bc-player-examples">This page on GitLab</a><br />Get the full source code and play with it.</p>
    	    	</div>
	        	<div class="large-3 medium-3 columns">
	        		<p><a href="https://github.com/BrightcoveLearning/BCL-LearningSamples">Brightcove Learning Samples on Github</a><br />Samples and other files created for external or internal customers.</p>
	        	</div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://support.brightcove.com/en/video-cloud/docs/setting-videos-not-hosted-brightcove-play-within-facebook">Setting Videos not Hosted on Brightcove to Play within Facebook</a><br />Try sharing this video on Facebook. It should be playable right in your timeline, have all the metadata and link back here.</p>
            </div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/video-cloud/index.html">Developer Docs</a><br />If you're developer digging deep into our APIs, check out or Dev Docs. They're crazy good.</p>
            </div>
					</div>
      	</div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <p class="disclaimer">This page was created by <a href="mailto:prebrov@brightcove.com?Subject=From%20your%20Sample%20Pages" target="_blank">Pavel Rebrov</a> and uses <a href="http://foundation.zurb.com" title="Zurb Foundation">Zurb Foundation</a> and some of its dependencies. Brightcove Player doesn't require any of it.</p>
      </div>
    </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/menu.js"></script>
    <script src="../player-plugins/videojs-contrib-ads/videojs.ads.js"></script>
    <script src="../player-plugins/vast-client/vast-client.js"></script>
    <script src="../player-plugins/videojs-vast/videojs.vast.js"></script>
    <script>
      $(document).foundation();

      var example_video_1 = videojs('example_video_1');

      example_video_1.ads();
      example_video_1.vast({
        url: 'http://videoads.theonion.com/vast/270.xml'
//        url: 'http://pubads.g.doubleclick.net/gampad/ads?adk=2784679071&ciu_szs=300x250%2C728x90&correlator=785946219905024&dt=1451982842114&eid=291885057&env=vp&flash=20.0.0.267&frm=0&gdfp_req=1&ged=ta0_ve3_pt5.u.5_td5_tv1_is0_er220.164.742.1092_sv2_sp1_vi0.0.460.1255_vp46_ct1_vb0_vl1_vr1_eb23144&impl=s&iu=%2F6062%2Fiab_vast_samples%2Fskippable&mpt=brightcove%2Fplayer-fl&mpv=1.15.0&osd=6&output=xml_vast2&scor=1891061538488320&sdkv=3.220.0&sdr=1&sz=640x360&u_ah=873&u_asa=1&u_aw=1440&u_cd=24&u_h=900&u_his=24&u_java=false&u_nmime=7&u_nplug=5&u_tz=480&u_w=1440&unviewed_position_start=1&url=http%3A%2F%2Fsolutions.brightcove.com%2Fprebrov%2Fplayer-examples%2Fads-vast.php'
//        url: 'http://solutions.brightcove.com/prebrov/player-plugins/vastAd.xml'
      });
    </script>

  </body>
</html>
