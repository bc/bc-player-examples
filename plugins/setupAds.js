(function(window, document, vjs, undefined) {
  setupAds = function (options) {

    var myPlayer = this;
    var currentVideo = myPlayer.el().getAttribute('data-video-id');

    myPlayer.load(currentVideo);
    myPlayer.on("loadstart", function() {

      var adURL = options.adURL;
      console.log("AdURL1:"+ adURL);
      console.log("Mediainfo1:" + myPlayer.mediainfo);

      if (adURL.search("{categories}")) {
        var categories;
        for (var i = 0; i < myPlayer.mediainfo.tags.length; i++) {
          categories = myPlayer.mediainfo.tags[i] + "%2C";
        }
        adURL = adURL.replace("{categories}", encodeURIComponent(categories));
      }

      if (adURL.search("{mediaID}")){
          adURL = adURL.replace("{mediaID}", myPlayer.mediainfo.id );
      }

      if (adURL.search("{title}")){
          adURL = adURL.replace("{title}", encodeURIComponent(myPlayer.mediainfo.name));
      }

      if (adURL.search("{description}")){
          adURL = adURL.replace("{description}",  encodeURIComponent(myPlayer.mediainfo.description));
      }

      if (adURL.search("{mediaReferenceID}")){
          adURL = adURL.replace("{mediaReferenceID}", window.location.href);
      }

      if (adURL.search("[timestamp]")){
          adURL = adURL.replace("[timestamp]", (new Date).getTime());
      }

      if (adURL.search("{duration}")){
          adURL = adURL.replace("{duration}", myPlayer.mediainfo.duration);
      }

     var playerURL = "http://players.brightcove.net/" + myPlayer.el().getAttribute('data-account') + "/" + myPlayer.el().getAttribute('data-player') + "/index.html";

      if (adURL.search("[referrer_url]")){
          adURL = adURL.replace("[referrer_url]", playerURL);
      }

      if (adURL.search("[referrer_url]")){
          adURL = adURL.replace("[referrer_url]", document.referrer);
      }

      console.log("AdURL2:" + adURL);

      console.log("Calling ima3");
      myPlayer.ima3({
        "serverUrl": adURL,
        "debug": true,
        "prerollTimeout": 5000,
        "adTechOrder": ["flash","html5"]
      });
    });
  };
  videojs.plugin('setupAds', setupAds);
})(window, document, videojs);
