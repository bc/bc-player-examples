// Set active menu item based on the current path

$(function () {
    setActiveNav();
});

function setActiveNav() {

    var currentPath = location.href.toLowerCase();

    $('.top-bar-section ul li a').each(function() {
        if (currentPath.indexOf(this.href.toLowerCase()) > -1) {
            $("li.active").removeClass("active");
            $(this).parent().addClass("active");
        }
    });
}
