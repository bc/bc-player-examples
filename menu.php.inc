    <div class="contain-to-grid fixed">
      <nav class="top-bar" data-topbar role="navigation">

        <ul class="title-area">
          <li class="name logo">
            <h1><a href="./">Brightcove Player Demos</a></h1>
          </li>
           <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
          <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">
          <!-- Right Nav Section -->
          <ul class="right">
            <li class="has-dropdown">
              <a href="#">Brightcove Player</a>
              <ul class="dropdown">
                <li><a href="./">Responsive Player</a></li>
                <li><a href="facebook.php">Facebook Demo</a></li>
                <li><a href="bumper-iframe.php">Bumper videos</a></li>
                <li><a href="switches.php">Playrate</a></li>
                <li><a href="vertical.php">Vertical Player</a></li>
                <li><a href="drm.php">DRM Player</a></li>
                <li><a href="ads-ima3.php">Ad IMA3 Integration</a></li>
                <li><a href="adways.php">Adways</a></li>
                <li><a href="hapyak.php">HapYak</a></li>
<!--                 <li><a href="ads-vast.php">VAST Integration</a></li> -->
              </ul>
            </li>

            <li class="has-dropdown">
              <a href="#">Lift</a>
              <ul class="dropdown">
                <li><a href="lift-adblock.php">Overcoming Ad Blockers</a></li>
                <li><a href="lift-onceux.php">UX for server-side ads</a></li>
                <li><a href="perform.php">Own HLS + DFP Ads</a></li>
                <li><a href="perform-onceux.php">Perform &amp; Once UX</a></li>
                <li><a href="videojs-once.php">Video.js &amp; Once</a></li>
              </ul>
            </li>

            <li class="has-dropdown">
              <a href="#">Smart Player</a>
              <ul class="dropdown">
                <li><a href="smart-basic.php">Basic Smart Player</a></li>
                <li><a href="smart-responsive.php">Responsive Smart Player</a></li>
                <li><a href="smart-drm.php">DRM Smart Player</a></li>
              </ul>
            </li>
          </ul>
        </section>

      </nav>
    </div>
