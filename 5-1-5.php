<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Brightcove Player 5.1.5</title>

    <link rel="stylesheet" href="css/foundation.css" />

    <link rel="stylesheet" href="css/custom.css">

    <style type="text/css">
    .outer {
      width: 80%;
      display: block;
      position: relative;
      margin: 20px auto;
    }
    .outer:after {
      padding-top: 56.25%;
      display: block;
      content: '';
    }
    .video-js {
      position: absolute;
      top: 0;
      bottom: 0;
      right: 0;
      left: 0;
      width: 100% !important;
      height: 100% !important;
    }
    .video-js .vjs-playback-rate {
      order: 7; /* after the progress bar but before the volume button */
      -webkit-box-ordinal-group: 7;
      -moz-box-ordinal-group: 7;
      -webkit-order: 7;
      -ms-flex-order: 7;
      margin-bottom: 20px;
  }
  .video-js .vjs-playback-rate .vjs-control-content .vjs-menu-item, .video-js .vjs-captions-button .vjs-control-content .vjs-menu-item {
    color: white;
  }
  </style>

    <script src="js/vendor/modernizr.js"></script>
    <link rel="shortcut icon" href="favicon.ico">
  </head>

  <body>

<?php include_once("google-tag-manager.php.inc"); ?>

<?php include_once("menu-versions.php.inc"); ?>

    <div class="row">
      <div class="large-12 columns">
        <h1>Brightcove Player 5.1.5</h1>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
        <div class="panel">

            <div class="outer">

<video

  data-video-id="4757568891001"
  data-player="NkngTLp9x"

  data-account="4143377145001" data-embed="default" class="video-js" controls>
</video>
<script src="//players.brightcove.net/4143377145001/NkngTLp9x_default/index.min.js"></script>

              <p><div class="fb-like" data-send="false" data-width="450" data-show-faces="true" data-colorscheme="dark" data-font="trebuchet ms"></div></p>

            </div>

          <p>Feel free to explore the source code of this page.</p>
        </div>

      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <p class="disclaimer">This page was created by <a href="mailto:prebrov@brightcove.com?Subject=From%20your%20Sample%20Pages" target="_blank">Pavel Rebrov</a> and uses <a href="http://foundation.zurb.com" title="Zurb Foundation">Zurb Foundation</a> and some of its dependencies. Brightcove Player doesn't require any of it.</p>
      </div>
    </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/menu.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
