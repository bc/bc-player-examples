<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Once Demos | Brightcove Perform</title>

    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css">

    <link href="css/video-js.min.css" rel="stylesheet" />

    <script src="js/vendor/modernizr.js"></script>

    <link rel="shortcut icon" href="favicon.ico">

    <style>
      .video-js { padding-top: 56.25% }
      .vjs-fullscreen { padding-top: 0px }
      .outer { min-width: 410px; }
      .outer:after { padding-top: 0; }
      .row {
         width: 100%;
         margin-left: auto;
         margin-right: auto;
         max-width: 80rem;
      }
    </style>
  </head>

  <body>

<?php include_once("google-tag-manager.php.inc"); ?>

<?php include_once("menu.php.inc"); ?>

    <div class="row">
      <div class="large-12 columns">
        <h1>Brightcove Lift: Once UX</h1>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
      	<div class="panel">
	        <h3></h3>

          <div class="row">
            <div class="large-6 medium-6 columns">
              <p>Ads on this player are integrated server-side. <b>Once UX plugin enables user interaction</b> with these ads.</p>
            </div>
            <div class="large-6 medium-6 columns">
              <p>Same stream, ads are integrated server-side. Ads are passing through Ad Blocking, but Open Source player <b>doesn't have support for UX</b>.</p>
            </div>
          </div>

          <div class="row">
            <div class="large-6 medium-6 columns">
              <div class="outer">

                <iframe src="//players.brightcove.net/4165107283001/Bk6nKQSH_default/index.html" allowfullscreen webkitallowfullscreen mozallowfullscreen style="width: 100%; height: 333px;"></iframe>

              </div>
            </div>

            <div class="large-6 medium-6 columns">
              <div class="outer">

                <video id="videojs-once" poster="img/html5.jpg"
                class="video-js vjs-default-skin" width="auto" height="auto" controls preload="auto" data-setup="{}">

                <source src="http://once.unicornmedia.com/now/master/playlist/ffccfeab-bcb1-401d-82e9-e1639ea9fb6c/7d809c22-aa21-4d6d-bd7a-542fa72e39e8/c3b9babe-07f8-41bc-9d28-87a8a8a98a29/content.m3u8?UMPTPARAMcust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostoptimizedpod" type="application/x-mpegURL">

                <p class="vjs-no-js">
                  To view this video please enable JavaScript, and consider upgrading to a web browser that
                  <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                </p>
              </video>
              <script src="js/vendor/video.min.js"></script>
              <script src="js/vendor/videojs-contrib-hls.js"></script>

              </div>
            </div>
          </div>

          <p>Feel free to explore the source code of this page.</p>
        </div>

        <div class="panel">
          <h3>Learn more</h3>
          <form action="http://support.brightcove.com/en/video-cloud/search/"  accept-charset="UTF-8" method="post">
            <div class="row">
              <div class="large-12 columns">
                <div class="row collapse">
                  <div class="large-10 columns">
                    <input type="text" placeholder="Have a question about Brightcove?" name="search_block_form">
                  </div>
                  <div class="large-2 columns">
                    <button type="submit" class="button prefix">Search</button>
                    <input type="hidden" name="form_build_id" id="form-1b2d71d83ffa155abd063cae196a7104" value="form-1b2d71d83ffa155abd063cae196a7104"  />
                    <input type="hidden" name="form_id" id="edit-search-block-form" value="search_block_form"  />
                  </div>
                </div>
              </div>
            </div>
          </form>

	        <div class="row">
	        	<div class="large-3 medium-3 columns">
    	    		<p><a href="https://gitlab.com/bc/bc-player-examples">This page on GitLab</a><br />Get the full source code and play with it.</p>
    	    	</div>
	        	<div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/once/guides/once-vod-2-0.html">Getting started with Once</a><br />Deep dive into developer documentation for Once, Brightcove's Server Side Ad Insertion platform.</p>
	        	</div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/video-cloud/brightcove-player/guides/onceux-plugin.html">Advertising with the Once UX Plugin</a><br />Once UX provides User Interface to handle ad skippability, countdowns, control playback, etc. for Server Side Ad Insertion.</p>
            </div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/perform/brightcove-player/guides/ima-plugin.html">Advertising with IMA3 Plugin Guide</a><br />The IMA plugin integrates the Brightcove player with Google's Interactive Media Ads. This allows you to request and track VAST ads for your player.</p>
            </div>
					</div>
      	</div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <p class="disclaimer">This page was created by <a href="mailto:prebrov@brightcove.com?Subject=From%20your%20Sample%20Pages" target="_blank">Pavel Rebrov</a> and uses <a href="http://foundation.zurb.com" title="Zurb Foundation">Zurb Foundation</a> and some of its dependencies. Brightcove Player doesn't require any of it.</p>
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/menu.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
