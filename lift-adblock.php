<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Once Demos | Brightcove Perform</title>

    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css">

    <script src="js/vendor/modernizr.js"></script>

    <link rel="shortcut icon" href="favicon.ico">

    <style>
      .outer { min-width: 410px; }
      .outer:after { padding-top: 0; }
      .row {
         width: 100%;
         margin-left: auto;
         margin-right: auto;
         max-width: 80rem;
      }
    </style>
  </head>

  <body>

<?php include_once("google-tag-manager.php.inc"); ?>

<?php include_once("menu.php.inc"); ?>

    <div class="row fullWidth">
      <div class="large-12 columns">
        <h1>Brightcove Lift: Ad Blockers</h1>
      </div>
    </div>

    <div class="row fullWidth">
      <div class="large-12 columns">
      	<div class="panel">
	        <h3>Overcoming Ad Blockers</h3>

          <div class="row">
            <div class="large-6 medium-6 columns">
              <p>Ads on this player are integrated server-side. <b>Ad Blocker will&nbsp;not see them</b>. People will.</p>
            </div>
            <div class="large-6 medium-6 columns">
              <p>Ads are integrated client-side. <b>Ad Blocker will kill them.</b></p>
            </div>
          </div>

          <div class="row">
            <div class="large-6 medium-6 columns">
              <div class="outer">

                <iframe src="//players.brightcove.net/4165107283001/d9fc2140-e484-4337-91e2-91d191e149d8_default/index.html" allowfullscreen webkitallowfullscreen mozallowfullscreen style="width: 100%; height: 333px;"></iframe>

              </div>
            </div>

            <div class="large-6 medium-6 columns">
              <div class="outer">

                <iframe src="//players.brightcove.net/4165107283001/N1ecHZPC9g_default/index.html" allowfullscreen webkitallowfullscreen mozallowfullscreen style="width: 100%; height: 333px;"></iframe>

              </div>
            </div>
          </div>

          <p>Feel free to explore the source code of this page.</p>
        </div>

        <div class="panel">
          <h3>Learn more</h3>
          <form action="http://support.brightcove.com/en/video-cloud/search/"  accept-charset="UTF-8" method="post">
            <div class="row">
              <div class="large-12 columns">
                <div class="row collapse">
                  <div class="large-10 columns">
                    <input type="text" placeholder="Have a question about Brightcove?" name="search_block_form">
                  </div>
                  <div class="large-2 columns">
                    <button type="submit" class="button prefix">Search</button>
                    <input type="hidden" name="form_build_id" id="form-1b2d71d83ffa155abd063cae196a7104" value="form-1b2d71d83ffa155abd063cae196a7104"  />
                    <input type="hidden" name="form_id" id="edit-search-block-form" value="search_block_form"  />
                  </div>
                </div>
              </div>
            </div>
          </form>

	        <div class="row">
	        	<div class="large-3 medium-3 columns">
    	    		<p><a href="https://gitlab.com/bc/bc-player-examples">This page on GitLab</a><br />Get the full source code and play with it.</p>
    	    	</div>
	        	<div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/once/guides/once-vod-2-0.html">Getting started with Once</a><br />Deep dive into developer documentation for Once, Brightcove's Server Side Ad Insertion platform.</p>
	        	</div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/video-cloud/brightcove-player/guides/onceux-plugin.html">Advertising with the Once UX Plugin</a><br />Once UX provides User Interface to handle ad skippability, countdowns, control playback, etc. for Server Side Ad Insertion.</p>
            </div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/perform/brightcove-player/guides/ima-plugin.html">Advertising with IMA3 Plugin Guide</a><br />The IMA plugin integrates the Brightcove player with Google's Interactive Media Ads. This allows you to request and track VAST ads for your player.</p>
            </div>
					</div>
      	</div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <p class="disclaimer">This page was created by <a href="mailto:prebrov@brightcove.com?Subject=From%20your%20Sample%20Pages" target="_blank">Pavel Rebrov</a> and uses <a href="http://foundation.zurb.com" title="Zurb Foundation">Zurb Foundation</a> and some of its dependencies. Brightcove Player doesn't require any of it.</p>
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/menu.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
