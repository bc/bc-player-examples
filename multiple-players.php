
<html>
<head>
<title>Brightcove Player Playlists</title>
<link rel="stylesheet" type="text/css" title="vertical" href="css/vertical.css" />
<link rel="stylesheet" type="text/css" title="horizontal" href="css/horizontal.css" disabled />
<link rel="stylesheet" type="text/css" title="stack" href="css/stack.css" disabled />

</head>
<body>

  <?php include_once("google-tag-manager.php.inc"); ?>

<div class="myplayer">
  <video id="example_video_1"
    data-playlist-id="4341870898001"
    data-account="4143377145001"
    data-player="751f185c-063f-4c16-b44b-36de30687e84"
    data-embed="default"
    class="video-js" controls></video>
  <div class="playlist-wrapper">
   <ol class="vjs-playlist"></ol>
  </div>
</div>

<p>&nbsp;</p>

<div class="myplayer">
  <video id="example_video_2"
    data-video-id="4147573784001"
    data-account="4143377145001"
    data-player="751f185c-063f-4c16-b44b-36de30687e84"
    data-embed="default"

    class="video-js" controls></video>
</div>

<script src="//players.brightcove.net/4143377145001/751f185c-063f-4c16-b44b-36de30687e84_default/index.min.js"></script>

 </body>
</html>


