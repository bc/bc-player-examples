<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DRM-enabled Smart Player</title>

    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css">

<!-- Stretch the player 100% of container -->
    <style type="text/css" media="screen">
     .BrightcoveExperience {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
    </style>

    <script src="js/vendor/modernizr.js"></script>

    <link rel="shortcut icon" href="favicon.ico">
  </head>

  <body>

<?php include_once("google-tag-manager.php.inc"); ?>

<?php include_once("menu.php.inc"); ?>

    <div class="row">
      <div class="large-12 columns">
        <h1>Smart Player with DRM protection</h1>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
      	<div class="panel">
	        <h3>Amazing to see you here!</h3>
	        <p>Try stealing this video!</p>

<!-- Wrap Brightcove Player into a container with an intrinsic ratio
http://docs.brightcove.com/en/video-cloud/smart-player-api/samples/responsive-sizing.html
-->
<div class="video-container">

<!-- Start of Brightcove Player -->

<div style="display:none">

</div>

<!-- Start of Brightcove Player -->

<div style="display:none">

</div>

<!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at https://accounts.brightcove.com/en/terms-and-conditions/.
-->

<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>

<object id="myExperience4679281278001" class="BrightcoveExperience">
  <param name="bgcolor" value="#FFFFFF" />
  <param name="width" value="480" />
  <param name="height" value="270" />
  <param name="playerID" value="4255357862001" />
  <param name="playerKey" value="AQ~~,AAAD3bdlVFk~,-MmDkYcwgZkwx02HpIhjOGQyHrfmx9nf" />
  <param name="isVid" value="true" />
  <param name="isUI" value="true" />
  <param name="dynamicStreaming" value="true" />

  <param name="@videoPlayer" value="4679281278001" />
</object>

<!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.
-->
<script type="text/javascript">brightcove.createExperiences();</script>

<!-- End of Brightcove Player -->

</div>

          <p>Feel free to explore the source code of this page.</p>
        </div>

        <div class="panel">
          <h3>Learn more</h3>
          <form action="http://support.brightcove.com/en/video-cloud/search/"  accept-charset="UTF-8" method="post">
            <div class="row">
              <div class="large-12 columns">
                <div class="row collapse">
                  <div class="large-10 columns">
                    <input type="text" placeholder="Have a question about Brightcove?" name="search_block_form">
                  </div>
                  <div class="large-2 columns">
                    <button type="submit" class="button prefix">Search</button>
                    <input type="hidden" name="form_build_id" id="form-1b2d71d83ffa155abd063cae196a7104" value="form-1b2d71d83ffa155abd063cae196a7104"  />
                    <input type="hidden" name="form_id" id="edit-search-block-form" value="search_block_form"  />
                  </div>
                </div>
              </div>
            </div>
          </form>

	        <div class="row">
	        	<div class="large-3 medium-3 columns">
    	    		<p><a href="https://gitlab.com/bc/bc-player-examples">This page on GitLab</a><br />Get the full source code and play with it.</p>
    	    	</div>
	        	<div class="large-3 medium-3 columns">
	        		<p><a href="https://github.com/BrightcoveLearning/BCL-LearningSamples">Brightcove Learning Samples on Github</a><br />Samples and other files created for external or internal customers.</p>
	        	</div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://support.brightcove.com/en/video-cloud/docs/topics/3331">Creating & Customizing Players</a><br />Check out our best in industry guides on customizing players in browser.</p>
            </div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/video-cloud/index.html">Developer Docs</a><br />If you're developer digging deep into our APIs, check out or Dev Docs. They're crazy good.</p>
            </div>
					</div>
      	</div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <p class="disclaimer">This page was created by <a href="mailto:prebrov@brightcove.com?Subject=From%20your%20Sample%20Pages" target="_blank">Pavel Rebrov</a> and uses <a href="http://foundation.zurb.com" title="Zurb Foundation">Zurb Foundation</a> and some of its dependencies. Brightcove Player doesn't require any of it.</p>
      </div>
    </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/menu.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
