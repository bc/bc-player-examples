<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Just Once | Brightcove Perform</title>

    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css">

    <link href="css/video-js.min.css" rel="stylesheet" />

    <style>
      .video-js { padding-top: 56.25% }
      .vjs-fullscreen { padding-top: 0px }
      .outer:after { padding-top: 0 }
    </style>

    <script src="js/vendor/modernizr.js"></script>

    <link rel="shortcut icon" href="favicon.ico">
  </head>

  <body>

<?php include_once("google-tag-manager.php.inc"); ?>

<?php include_once("menu.php.inc"); ?>

    <div class="row">
      <div class="large-12 columns">
        <h1>Brightcove Once</h1>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
        <div class="panel">
          <h3>Video.js and Once</h3>
          <p>Here's video.js, world's most popular open source html5 video player, playing a VOD with pre-rolls, mid-rolls and a post-roll stitched on the server.</p>

            <div class="outer">

              <video id="videojs-once" poster="img/html5.jpg"
                class="video-js vjs-default-skin" width="auto" height="auto" controls preload="auto" data-setup="{}">

                <source src="http://once.unicornmedia.com/now/master/playlist/4236fac6-f0df-4f9a-b9c7-159cfb257618/e4dc40a6-afd5-40ab-b868-82797fe8164f/93ebee84-0b53-4671-a4e1-a5c95b083a0d/content.m3u8" type="application/x-mpegURL">

                <p class="vjs-no-js">
                  To view this video please enable JavaScript, and consider upgrading to a web browser that
                  <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                </p>
              </video>
              <script src="js/vendor/video.min.js"></script>
              <script src="js/vendor/videojs-contrib-hls.js"></script>

              <p><div class="fb-like" data-send="false" data-width="450" data-show-faces="true" data-colorscheme="dark" data-font="trebuchet ms"></div></p>

            </div>

          <p>Feel free to explore the source code of this page.</p>
        </div>

        <div class="panel">
          <h3>Learn more</h3>
          <form action="http://support.brightcove.com/en/video-cloud/search/"  accept-charset="UTF-8" method="post">
            <div class="row">
              <div class="large-12 columns">
                <div class="row collapse">
                  <div class="large-10 columns">
                    <input type="text" placeholder="Have a question about Brightcove?" name="search_block_form">
                  </div>
                  <div class="large-2 columns">
                    <button type="submit" class="button prefix">Search</button>
                    <input type="hidden" name="form_build_id" id="form-1b2d71d83ffa155abd063cae196a7104" value="form-1b2d71d83ffa155abd063cae196a7104"  />
                    <input type="hidden" name="form_id" id="edit-search-block-form" value="search_block_form"  />
                  </div>
                </div>
              </div>
            </div>
          </form>

          <div class="row">
            <div class="large-6 medium-6 columns">
              <p><a href="https://gitlab.com/bc/bc-player-examples">This page on GitLab</a><br />Get the full source code and play with it.</p>
            </div>
            <div class="large-6 medium-6 columns">
              <p><a href="http://docs.brightcove.com/en/once/guides/once-vod-2-0.html">Getting started with Once</a><br />Deep dive into developer documentation for Once, Brightcove's Server Side Ad Insertion platform.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <p class="disclaimer">This page was created by <a href="mailto:prebrov@brightcove.com?Subject=From%20your%20Sample%20Pages" target="_blank">Pavel Rebrov</a> and uses <a href="http://foundation.zurb.com" title="Zurb Foundation">Zurb Foundation</a> and some of its dependencies. Brightcove Player doesn't require any of it.</p>
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/menu.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
