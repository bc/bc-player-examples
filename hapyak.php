<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta name="twitter:card" content="player">
    <meta name="twitter:site" content="@bc_poster">

    <meta name="twitter:title" content="BC HapYak | Twitter share test">
    <meta name="twitter:description" content="You should be able to play this video in line!">
    <meta name="twitter:image" content="//brightcove04.o.brightcove.com/4143377145001/4143377145001_5019161203001_5019149703001-vs.jpg?pubId=4143377145001&videoId=5019149703001">
    <meta name="twitter:image:width" content="480">
    <meta name="twitter:image:height" content="270">

    <meta name="twitter:player" content="https://solutions.brightcove.com/prebrov/player-examples/hapyak.php">
    <meta name="twitter:player:width" content="480">
    <meta name="twitter:player:height" content="270">

    <meta name="twitter:url" content=”https://solutions.brightcove.com/prebrov/player-examples/hapyak.php″>

    <title>Brightcove Player with HapYak plugin</title>

    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css">

    <script src="js/vendor/modernizr.js"></script>
    <link rel="shortcut icon" href="favicon.ico">
  </head>

  <body>

<?php include_once("google-tag-manager.php.inc"); ?>

<?php include_once("menu.php.inc"); ?>

    <div class="row">
      <div class="large-12 columns">
        <h1>Interactive video</h1>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
      	<div class="panel">
	        <h3>Brightcove &amp; HapYak</h3>


            <div class="outer">

<div style="display: block; position: relative; max-width: 100%;"><div style="padding-top: 56.25%;">

<video id="hapyak_video"
   data-account="4143377145001"
   data-player="default"
   data-embed="default"
   data-video-id="5079142017001"
   class="video-js" playsinline="playsinline" controls
   style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px">
</video>
<script src="//players.brightcove.net/4143377145001/default_default/index.min.js"></script>

<script type="text/javascript" src="//d2qrdklrsxowl2.cloudfront.net/js/hapyak.js"></script>
<script type="text/javascript">
(function () {
    var vjs = videojs("hapyak_video");
    vjs.one("loadedmetadata", function () {
      hapyak.viewer({
        apiKey: "75bb0cd84f0f4b0ba879",
        projectId: 42086,
        plugins: {
          annotationSources: {"brightcove.cuepoints": true}
        },
        resetVariables: true,
        player: vjs,
        playerType: "brightcove-v2",
        autoplay: false
      });
    });
}());
</script>

</div></div>


              <p><div class="fb-like" data-send="false" data-width="450" data-show-faces="true" data-colorscheme="dark" data-font="trebuchet ms"></div></p>

            </div>

          <p>Feel free to explore the source code of this page.</p>
        </div>

        <div class="panel">
          <h3>Learn more</h3>
          <form action="http://support.brightcove.com/en/video-cloud/search/"  accept-charset="UTF-8" method="post">
            <div class="row">
              <div class="large-12 columns">
                <div class="row collapse">
                  <div class="large-10 columns">
                    <input type="text" placeholder="Have a question about Brightcove?" name="search_block_form">
                  </div>
                  <div class="large-2 columns">
                    <button type="submit" class="button prefix">Search</button>
                    <input type="hidden" name="form_build_id" id="form-1b2d71d83ffa155abd063cae196a7104" value="form-1b2d71d83ffa155abd063cae196a7104"  />
                    <input type="hidden" name="form_id" id="edit-search-block-form" value="search_block_form"  />
                  </div>
                </div>
              </div>
            </div>
          </form>

	        <div class="row">
	        	<div class="large-3 medium-3 columns">
    	    		<p><a href="https://gitlab.com/bc/bc-player-examples">This page on GitLab</a><br />Get the full source code and play with it.</p>
    	    	</div>
	        	<div class="large-3 medium-3 columns">
	        		<p><a href="https://github.com/BrightcoveLearning/BCL-LearningSamples">Brightcove Learning Samples on Github</a><br />Samples and other files created for external or internal customers.</p>
	        	</div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://support.brightcove.com/en/video-cloud/docs/setting-videos-not-hosted-brightcove-play-within-facebook">Setting Videos not Hosted on Brightcove to Play within Facebook</a><br />Try sharing this video on Facebook. It should be playable right in your timeline, have all the metadata and link back here.</p>
            </div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/video-cloud/index.html">Developer Docs</a><br />If you're developer digging deep into our APIs, check out or Dev Docs. They're crazy good.</p>
            </div>
					</div>
      	</div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <p class="disclaimer">This page was created by <a href="mailto:prebrov@brightcove.com?Subject=From%20your%20Sample%20Pages" target="_blank">Pavel Rebrov</a> and uses <a href="http://foundation.zurb.com" title="Zurb Foundation">Zurb Foundation</a> and some of its dependencies. Brightcove Player doesn't require any of it.</p>
      </div>
    </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/menu.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
