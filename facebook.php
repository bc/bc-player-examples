<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Facebook embeddable player</title>

    <meta property="fb:admins" content="100009441878541" />

    <meta property="og:title" content="Caminandes: Gran Dillama"/>
    <meta property="og:description" content="(c) copyright 2014, Blender Foundation / www.caminandes.com"/>
    <meta property="og:url" content="http://solutions.brightcove.com/prebrov/player-examples/facebook.php"/>

    <meta property="og:type" content="movie"/>
    <meta property="og:video:width" content="480" />
    <meta property="og:video:height" content="270" />

    <meta property="og:video"  content="http://c.brightcove.com/services/viewer/federated_f9/?isVid=1&isUI=1&playerID=4143306065001&autoStart=true&videoID=4147573784001"/>
    <meta property="og:video:secure_url"  content="https://secure.brightcove.com/services/viewer/federated_f9/?isVid=true&isUI=true&playerID=4143306065001&secureConnections=true&videoID=4147573784001&autoStart=true"/>
    <meta property="og:video:type" content="application/x-shockwave-flash"/>
    <meta property="og:video"  content="http://brightcove04.brightcove.com/22/4143377145001/201506/1572/4143377145001_4329716976001_4147573784001.mp4?pubId=4143377145001&videoId=4147573784001"/>
    <meta property="og:video:type" content="video/mp4"/>

    <meta property="og:image" content="http://brightcove04.o.brightcove.com/4143377145001/4143377145001_4329724000001_4147573784001-vs.jpg?pubId=4143377145001&videoId=4147573784001"/>


    <meta name="twitter:card" content="player">

    <meta name="twitter:title" content="BC Tester | Twitter share test">
    <meta name="twitter:description" content="You should be able to play this video in line!">
    <meta name="twitter:image" content="http://brightcove04.o.brightcove.com/4143377145001/4143377145001_4329724000001_4147573784001-vs.jpg?pubId=4143377145001&videoId=4147573784001">
    <meta name="twitter:image:width" content="480">
    <meta name="twitter:image:height" content="270">

    <meta name="twitter:player" content="https://players.brightcove.net/4143377145001/2b381371-8804-4e70-8627-fdacf97a685c_default/index.html?videoId=4147573784001">
    <meta name="twitter:player:width" content="480">
    <meta name="twitter:player:height" content="270">

    <meta name="twitter:player:stream" content="http://brightcove04.brightcove.com/22/4143377145001/201506/1572/4143377145001_4329716976001_4147573784001.mp4?pubId=4143377145001&videoId=4147573784001">
    <meta name="twitter:player:stream:content_type" content="video/mp4; codecs=&quot;avc1.42E01E1, mp4a.40.2&quot;">

    <meta name="twitter:url" content=”http://solutions.brightcove.com/prebrov/player-examples/facebook.php″>


    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css">

    <script src="js/vendor/modernizr.js"></script>

    <link rel="shortcut icon" href="favicon.ico">
  </head>

  <body>

<?php include_once("google-tag-manager.php.inc"); ?>

<?php include_once("menu.php.inc"); ?>

    <div class="row">
      <div class="large-12 columns">
        <h1>Facebook embeddable player</h1>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
      	<div class="panel">
	        <h3>Amazing to see you here!</h3>
	        <p>Try sharing this page on Facebook. Check out the source code for OpenGraph tags.</p>

          <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

<!-- Wrap Brightcove Player into a container with an intrinsic ratio
http://docs.brightcove.com/en/video-cloud/smart-player-api/samples/responsive-sizing.html
-->
  <div class="outer">

  <video
    data-video-id="4147573784001"
    data-account="4143377145001"
    data-player="2b381371-8804-4e70-8627-fdacf97a685c"
    data-embed="default"
    class="video-js" controls></video>
  <script src="//players.brightcove.net/4143377145001/2b381371-8804-4e70-8627-fdacf97a685c_default/index.min.js"></script>


</div>

          <p><div class="fb-like" data-send="false" data-width="450" data-show-faces="true" data-colorscheme="dark" data-font="trebuchet ms"></div></p>

          <p>Feel free to explore the source code of this page.</p>
        </div>

        <div class="panel">
          <h3>Learn more</h3>
          <form action="http://support.brightcove.com/en/video-cloud/search/"  accept-charset="UTF-8" method="post">
            <div class="row">
              <div class="large-12 columns">
                <div class="row collapse">
                  <div class="large-10 columns">
                    <input type="text" placeholder="Have a question about Brightcove?" name="search_block_form">
                  </div>
                  <div class="large-2 columns">
                    <button type="submit" class="button prefix">Search</button>
                    <input type="hidden" name="form_build_id" id="form-1b2d71d83ffa155abd063cae196a7104" value="form-1b2d71d83ffa155abd063cae196a7104"  />
                    <input type="hidden" name="form_id" id="edit-search-block-form" value="search_block_form"  />
                  </div>
                </div>
              </div>
            </div>
          </form>

	        <div class="row">
	        	<div class="large-3 medium-3 columns">
    	    		<p><a href="https://gitlab.com/bc/bc-player-examples">This page on GitLab</a><br />Get the full source code and play with it.</p>
    	    	</div>
	        	<div class="large-3 medium-3 columns">
	        		<p><a href="https://github.com/BrightcoveLearning/BCL-LearningSamples">Brightcove Learning Samples on Github</a><br />Samples and other files created for external or internal customers.</p>
	        	</div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://support.brightcove.com/en/video-cloud/docs/setting-videos-not-hosted-brightcove-play-within-facebook">Setting Videos not Hosted on Brightcove to Play within Facebook</a><br />Try sharing this video on Facebook. It should be playable right in your timeline, have all the metadata and link back here.</p>
            </div>
            <div class="large-3 medium-3 columns">
              <p><a href="http://docs.brightcove.com/en/video-cloud/index.html">Developer Docs</a><br />If you're developer digging deep into our APIs, check out or Dev Docs. They're crazy good.</p>
            </div>
					</div>
      	</div>
      </div>
    </div>

    <div class="row">
      <div class="small-12 columns">
        <p class="disclaimer">This page was created by <a href="mailto:prebrov@brightcove.com?Subject=From%20your%20Sample%20Pages" target="_blank">Pavel Rebrov</a> and uses <a href="http://foundation.zurb.com" title="Zurb Foundation">Zurb Foundation</a> and some of its dependencies. Brightcove Player doesn't require any of it.</p>
      </div>
    </div>


    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/menu.js"></script>
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
